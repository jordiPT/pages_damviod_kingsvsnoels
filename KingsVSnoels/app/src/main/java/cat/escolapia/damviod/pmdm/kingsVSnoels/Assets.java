package cat.escolapia.damviod.pmdm.kingsVSnoels;

import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

/**
 * Created by jordi.pages on 21/12/2016.
 */
public class Assets {

    public static Pixmap background;
    public static Pixmap backgroundBlack;
    public static Pixmap BackgroundMM;
    public static Pixmap logo;
    public static Pixmap MainMenu;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap king;
    public static Pixmap king2;
    public static Pixmap king3;
    public static Pixmap noel;
    public static Pixmap stars1;
    public static Pixmap stars2;
    public static Pixmap stars3;
    public static Pixmap youGot;
    public static Pixmap menuGO;
    public static Pixmap buttonUp;
    public static Pixmap buttonLeft;
    public static Pixmap buttonRight;
    public static Pixmap backButton;
    public static Pixmap tapAnyWhere;
    public static Pixmap menuPaused;
}
