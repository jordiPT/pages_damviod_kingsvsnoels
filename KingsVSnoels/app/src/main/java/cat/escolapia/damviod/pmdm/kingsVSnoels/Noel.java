package cat.escolapia.damviod.pmdm.kingsVSnoels;

import java.util.Random;

import cat.escolapia.damviod.pmdm.framework.Pixmap;

/**
 * Created by Jordi on 18/1/2017.
 */

public class Noel {
    public World world;

    public int x;
    public int y;
    public int id;
    public Random random = new Random();

    public Noel(int pos_y, int  _id) {

        id = _id;
        y = pos_y;
        x = random.nextInt(10) + 1;
    }

    public void goLeft()
    {
        x--;
        if(x == 0)
        {
            x = 10;
        }
    }

    public void goRight()
    {
        x++;
        if(x == 10)
        {
            x = 0;
        }
    }

}
