package cat.escolapia.damviod.pmdm.kingsVSnoels;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Screen;

import static android.R.attr.width;
import static android.R.attr.x;
import static android.R.attr.y;

/**
 * Created by Jordi on 18/1/2017.
 */

public class MainMenuScreen extends Screen {

    public MainMenuScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {

        Graphics g = game.getGraphics();
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {

                if(inBounds(event, 84, 360, 150, 60) ) {
                    game.setScreen(new GameScreen(game));
                    return;
                }
            }
        }
    }

    private boolean inBounds(TouchEvent event,  int x, int y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1)
            return true;
        else
            return false;
    }


    @Override
    public void render(float deltaTime) {

        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.BackgroundMM, 0, 0);
        g.drawPixmap(Assets.MainMenu, 64, 350);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
