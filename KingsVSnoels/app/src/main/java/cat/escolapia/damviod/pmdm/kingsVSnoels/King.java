package cat.escolapia.damviod.pmdm.kingsVSnoels;

import cat.escolapia.damviod.pmdm.framework.Pixmap;

/**
 * Created by jordi.pages on 21/12/2016.
 */
public class King {

    public World world;

    public int x;
    public int y;
    int type;

    public King(int paramType) {
        y = 13;
        x = (int)Math.floor(world.WORLD_WIDTH/2);

        type = paramType;
    }


    public void goUp() {
        y--;
    }
    public void goRight() { x++; }
    public void goLeft() { x--; }

    public boolean checkXoca(Noel noel) {

        int noel_x = noel.x;
        int noel_y = noel.y;


        if(noel_x == x && noel_y == y) {
            return true;
        }
        return false;

    }
}
