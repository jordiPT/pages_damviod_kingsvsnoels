package cat.escolapia.damviod.pmdm.kingsVSnoels;

import java.util.Random;

import cat.escolapia.damviod.pmdm.framework.Graphics;

/**
 * Created by jordi.pages on 21/12/2016.
 */
public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 12;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.05f;

    float tickTime = 0;
    float tick = TICK_INITIAL;
    public King currentKing;
    public Noel noel;
    int cont = 0; //nombre de vegades que ha colisionat
    public int score = 0;
    Noel noelList[] = new Noel[10];
    public boolean gameOver = false;
    public int kingsArrived = 0;

    public World()
    {
        for(int i = 0; i < 10; i++)
        {
            noel = new Noel(i+1, i+1);
            noelList[i] = noel;
        }

        currentKing = new King(0);
    }

    public void update(float deltaTime)
    {
        tickTime += deltaTime;

        while (tickTime > tick)
        {
            tickTime -= tick;

            if(currentKing.y == 0)
            {
                cont++;
                currentKing = new King(cont); //depenent del nombre del contador, cambiare el type dels reis
                tick -= TICK_DECREMENT;
                kingsArrived++;
            }

            //decremento la X dels noels
            for(int i = 0; i < noelList.length; i++)
            {
                if(noelList[i].id % 2 == 0)
                {
                    noelList[i].goLeft();
                }
                else
                {
                    noelList[i].goRight();
                }

            }

            //torno a comprovar si hi ha hagut colisió
            for(int z = 0; z < noelList.length; z++)
            {
                if (currentKing.checkXoca(noelList[z]))
                {
                    cont++;
                    currentKing = new King(cont); //depenent del nombre del contador, cambiare el type dels reis
                    tick -= TICK_DECREMENT;
                }
            }



            if(cont >2)
            {
               gameOver = true;
            }
            return;
        }
    }

}
