package cat.escolapia.damviod.pmdm.kingsVSnoels;

import android.graphics.Color;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by jordi.pages on 21/12/2016.
 */
public class GameScreen extends Screen {

    enum GameState {
        Ready,
        Running,
        Paused,
        GameOver
    }

    GameState state = GameState.Ready;
    World world;

    public GameScreen(Game game) {

        super(game);
        world = new World();
    }

    @Override
    public void update(float deltaTime)
    {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        if(state == GameState.Ready)
            updateReady(touchEvents);
        if(state == GameState.Running) {
            updateRunning(touchEvents, deltaTime);
            world.update(deltaTime);
        }
        if(state == GameState.Paused)
            updatePaused(touchEvents);
        if(state == GameState.GameOver)
            updateGameOver(touchEvents);


    }

    private void updateGameOver(List<TouchEvent> touchEvents)
    {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if (event.x > 70 && event.x < 70 + 192 && event.y > 200 && event.y < 250) {

                game.setScreen(new GameScreen(game));
            }
            if (event.x > 70 && event.x < 70 + 192 && event.y > 250 && event.y < 300) {

                game.setScreen(new MainMenuScreen(game));
            }
        }

    }

    private void drawGameOverUI() {
        Graphics g = game.getGraphics();

        if(world.kingsArrived == 0)
        {
            g.drawPixmap(Assets.gameOver, 80, 100);
            g.drawPixmap(Assets.menuGO, 70, 180);
        }

        if(world.kingsArrived == 1)
        {
            g.drawPixmap(Assets.youGot, 120, 100);
            g.drawPixmap(Assets.stars1, 120, 140);
            g.drawPixmap(Assets.menuGO, 70, 180);
        }

        if(world.kingsArrived == 2)
        {
            g.drawPixmap(Assets.youGot, 120, 100);
            g.drawPixmap(Assets.stars2, 120, 140);
            g.drawPixmap(Assets.menuGO, 70, 180);
        }

        if(world.kingsArrived == 3)
        {
            g.drawPixmap(Assets.youGot, 120, 100);
            g.drawPixmap(Assets.stars3, 120, 140);
            g.drawPixmap(Assets.menuGO, 70, 180);
        }

        g.drawLine(0, 416, 480, 416, Color.BLACK);
    }

    private void updateReady(List<TouchEvent> touchEvents) {
        if(touchEvents.size() > 0) state = GameState.Running;
    }

    private void drawReadyUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.backgroundBlack, 0, 0);
        g.drawPixmap(Assets.ready, 105, 210);
        g.drawPixmap(Assets.tapAnyWhere, 64, 260);
    }
    
    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++)
        {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_DOWN)
            {
                if (event.x > 70 && event.x < 70+192 && event.y > 200 && event.y < 250)
                {
                    state = GameState.Running;
                }

                if (event.x > 120 && event.x < 120+75 && event.y > 280 && event.y < 320)
                {
                    state = GameState.Ready;
                    game.setScreen(new MainMenuScreen(game));

                }
            }
        }
    }
    
    private void drawPausedUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.menuPaused, 70, 180);
    }


    
    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {

        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_DOWN) {
                if(event.x < 64 && event.y < 64) {
                    state = GameState.Paused;
                }
                if(event.x > 32 &&event.x < 64 &&  event.y > 416 && event.y < 448) {
                    world.currentKing.goUp();
                }
                if(event.x > 64 &&event.x < 96 &&  event.y > 448 && event.y < 480) {
                    world.currentKing.goRight();
                }
                if(event.x > 0 &&event.x < 32 &&  event.y > 448 && event.y < 480) {
                    world.currentKing.goLeft();
                }
                if(event.x > 320-33 && event.y > 480-33) {
                    game.setScreen(new MainMenuScreen(game));
                }

            }

        }

        world.update(deltaTime);

        if (world.gameOver) state = GameState.GameOver;

    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.background, 0, 0);


        drawWorld(world);

        if(state == GameState.Ready)
            drawReadyUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        if(state == GameState.GameOver)
            drawGameOverUI();
    }

    private void drawWorld(World world) {
        Graphics g = game.getGraphics();
        King king = world.currentKing;

        int x = king.x *32;
        int y = king.y *32;

        if(world.currentKing.type == 0)
        {g.drawPixmap(Assets.king, x, y);}
        if(world.currentKing.type == 1)
        {g.drawPixmap(Assets.king2, x, y);}
        if(world.currentKing.type == 2)
        {g.drawPixmap(Assets.king3, x, y);}

        for(int i = 0; i< world.noelList.length; i++)
        {
            g.drawPixmap(Assets.noel, world.noelList[i].x*32, world.noelList[i].y*32);
        }
    }

    private void drawRunningUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.pause, 5, 0);
        g.drawPixmap(Assets.buttonLeft, 0, 416+32);
        g.drawLine(0, 416, 480, 416, Color.WHITE);
        g.drawPixmap(Assets.buttonRight, 32+32, 416+32);
        g.drawPixmap(Assets.buttonUp, 32, 416);
        g.drawPixmap(Assets.backButton, 320-33, 480-33);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
