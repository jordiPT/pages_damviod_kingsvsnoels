package cat.escolapia.damviod.pmdm.kingsVSnoels;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by jordi.pages on 21/12/2016.
 */
public class LoadingScreen extends Screen {

    public LoadingScreen(Game game) {
        super(game);
    }


    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.BackgroundMM = g.newPixmap("BackgroundMM.png", Graphics.PixmapFormat.RGB565);
        Assets.background = g.newPixmap("background.png", Graphics.PixmapFormat.RGB565);
        Assets.backgroundBlack = g.newPixmap("backgroundBlack.png", Graphics.PixmapFormat.RGB565);
        Assets.menuGO = g.newPixmap("menuGO.png", Graphics.PixmapFormat.RGB565);
        Assets.logo = g.newPixmap("logo.png", Graphics.PixmapFormat.RGB565);
        Assets.stars1 = g.newPixmap("stars1.png", Graphics.PixmapFormat.RGB565);
        Assets.stars2 = g.newPixmap("stars2.png", Graphics.PixmapFormat.RGB565);
        Assets.stars3 = g.newPixmap("stars3.png", Graphics.PixmapFormat.RGB565);
        Assets.youGot = g.newPixmap("youGot.png", Graphics.PixmapFormat.RGB565);
        Assets.gameOver = g.newPixmap("gameOver.png", Graphics.PixmapFormat.RGB565);
        Assets.pause = g.newPixmap("pause.png", Graphics.PixmapFormat.RGB565);
        Assets.ready = g.newPixmap("ready.png", Graphics.PixmapFormat.RGB565);
        Assets.tapAnyWhere = g.newPixmap("tapAnyWhere.png", Graphics.PixmapFormat.RGB565);
        Assets.menuPaused = g.newPixmap("menuPaused.png", Graphics.PixmapFormat.RGB565);
        Assets.MainMenu = g.newPixmap("MainMenu.png", Graphics.PixmapFormat.RGB565);
        Assets.backButton = g.newPixmap("backButton.png", Graphics.PixmapFormat.RGB565);
        Assets.king = g.newPixmap("king.png", Graphics.PixmapFormat.RGB565);
        Assets.king2 = g.newPixmap("king2.png", Graphics.PixmapFormat.RGB565);
        Assets.king3 = g.newPixmap("king3.png", Graphics.PixmapFormat.RGB565);
        Assets.noel = g.newPixmap("noel.png", Graphics.PixmapFormat.RGB565);
        Assets.buttonLeft = g.newPixmap("buttonLeft.png", Graphics.PixmapFormat.RGB565);
        Assets.buttonRight = g.newPixmap("buttonRight.png", Graphics.PixmapFormat.RGB565);
        Assets.buttonUp = g.newPixmap("buttonUp.png", Graphics.PixmapFormat.RGB565);

        game.setScreen(new MainMenuScreen(game));
    }

    @Override
    public void render(float deltaTime) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
