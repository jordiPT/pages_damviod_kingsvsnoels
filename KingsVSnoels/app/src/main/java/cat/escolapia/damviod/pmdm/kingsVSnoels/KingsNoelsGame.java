package cat.escolapia.damviod.pmdm.kingsVSnoels;

import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.impl.AndroidGame;

/**
 * Created by jordi.pages on 21/12/2016.
 */
public class KingsNoelsGame extends AndroidGame {

    @Override
    public Screen getStartScreen() {
        return new LoadingScreen(this);
    }
}
